export function isDataResponse(response: JsonApiDocument): response is JsonApiDataDocument {
  return response && (response as JsonApiDataDocument).data !== undefined;
}

export function isErrorResponse(response: JsonApiDocument): response is JsonApiErrorDocument {
  return response && (response as JsonApiErrorDocument).errors !== undefined;
}

export interface JsonApiBaseDocument {
  jsonapi?: JsonApiObject;
  links?: JsonApiTopLevelLinksObject;
}

export interface JsonApiDataDocument extends JsonApiBaseDocument {
  data: JsonApiResourceObject & JsonApiResourceObject[];

  included?: JsonApiResourceObject[];
  meta?: JsonApiMetaObject;
}

export interface JsonApiErrorDocument extends JsonApiBaseDocument {
  errors: JsonApiErrorObject[];

  meta?: JsonApiMetaObject;
}

export interface JsonApiMetaDocument extends JsonApiBaseDocument {
  meta?: JsonApiMetaObject;
}

export type JsonApiDocument = JsonApiDataDocument | JsonApiErrorDocument | JsonApiMetaDocument;

export interface JsonApiErrorObject {
  code?: string;
  detail?: string;
  id?: string;
  links?: JsonApiErrorLinksObject;
  meta?: JsonApiMetaObject;
  status?: string;
  source?: {
    [ index: string ]: JsonApiLinksObjectMember;

    parameter?: string;
    pointer?: string;
  };
  title?: string;
}

export interface JsonApiLinksObject {
  [ index: string ]: JsonApiLinksObjectMember;
}

export type JsonApiLinksObjectMember = string | {
  href?: string;
  meta?: JsonApiMetaObject;
};

export interface JsonApiRelationshipsLinksObject extends JsonApiLinksObject {
  related?: JsonApiLinksObjectMember;
  self?: JsonApiLinksObjectMember;
}

export interface JsonApiErrorLinksObject extends JsonApiLinksObject {
  about: JsonApiLinksObjectMember;
}

export interface JsonApiTopLevelLinksObject extends JsonApiLinksObject {
  related?: JsonApiRelationshipsLinksObject;
  self?: JsonApiLinksObjectMember;

  // Pagination links
  first?: null | JsonApiLinksObjectMember;
  last?: null | JsonApiLinksObjectMember;
  prev?: null | JsonApiLinksObjectMember;
  next?: null | JsonApiLinksObjectMember;
}

export interface JsonApiObject {
  meta?: JsonApiMetaObject;
  version?: string;
}

export interface JsonApiAttributesObject {
  [ index: string ]: any;
}
export type JsonApiMetaObject = JsonApiAttributesObject;

export interface JsonApiRelationshipsObject {
  [ index: string ]: {
    data?: null | JsonApiResourceIdentifierObject | JsonApiResourceIdentifierObject[];
    links?: JsonApiRelationshipsLinksObject;
    meta?: JsonApiMetaObject;
  };
}

export interface JsonApiResourceObject {
  id: string;
  type: string;

  attributes?: JsonApiAttributesObject;
  relationships?: JsonApiRelationshipsObject;
  links?: JsonApiLinksObject;
  meta?: JsonApiMetaObject;
}

export interface JsonApiResourceIdentifierObject {
  id: string;
  type: string;

  meta?: JsonApiMetaObject;
}
