import "fetch-polyfill";
import { stringify as queryStringify } from "qs";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";

import "rxjs/add/observable/empty";
import "rxjs/add/observable/fromPromise";
import "rxjs/add/observable/of";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";

import {
  isDataResponse, isErrorResponse, JsonApiDataDocument, JsonApiDocument
} from "./jsonapi";
import {
  isStockMutationData, StockMutation, StockMutationData, StockMutationDetailData
} from "./stock_mutation";
import { isUnitData, Unit, UnitData } from "./unit";
import { UnitCategory, UnitCategoryData } from "./unit_category";
import { UnitClass, UnitClassData } from "./unit_class";
import { UnitType, UnitTypeData } from "./unit_type";

export class BacchusApi {
  public error$: Subject<Error> = new Subject();

  private _request: (
    endpoint: string, method: string, params: object, body: object
  ) => Promise<string>;

  constructor(private host: string, authorization_token: string = "") {
    this.setAuthorizationToken(authorization_token);
  }

  public createStockMutation(
    action: string,
    stock_mutation_details_attributes: StockMutationDetailData[] = [],
    metadata: object = {}
  ) {
    return this.request("/api/stock_mutations.json", "post", {}, {
      stock_mutation: { action, stock_mutation_details_attributes, metadata }
    }).map((response: JsonApiDocument) => {
      if (isDataResponse(response) && isStockMutationData(response.data)) {
        return new StockMutation(this, response.data);
      } else {
        this.error$.next(new Error("Did not receive appropriate data in response"));
      }
    });
  }

  public createUnit(unit_data: object) {
    return this.request("/api/units.json", "post", {}, {
      unit: unit_data
    }).map((response: JsonApiDocument) => {
      if (isDataResponse(response) && isUnitData(response.data)) {
        return new Unit(this, response.data);
      } else {
        this.error$.next(new Error("Did not receive appropriate data in response"));
      }
    });
  }

  public deleteStockMutation(id: number) {
    return this.request(`/api/stock_mutations/${id}.json`, "delete")
      .map((response: JsonApiDocument) => !!response);
  }

  public request(
    endpoint: string = "",
    method: string = "get",
    params: object = {},
    body: object = {}
  ) {
    return Observable.fromPromise(this._request(endpoint, method, params, body))
      .catch((error) => {
        this.error$.next(error);
        return Observable.empty();
      }).mergeMap((response: JsonApiDocument) => {
        if (isErrorResponse(response)) {
          response.errors.forEach((error) => {
            this.error$.next(new Error(error.title));
          });
          return Observable.empty();
        } else if (!isDataResponse(response)) {
          this.error$.next(new Error("Did not receive appropriate data in response"));
          return Observable.empty();
        } else {
          return Observable.of(response);
        }
      });
  }

  public sell(details: StockMutationDetailData[] = [], metadata: object = {}) {
    return this.createStockMutation("sold", details, metadata);
  }

  public setAuthorizationToken(authorization_token: string) {
    this._request = async (
      endpoint: string = "",
      method: string = "get",
      params: object = {},
      body: object = {}
    ) => {
      let uri = `${this.host}${endpoint}`;

      const payload = {
        headers: {
          Accept: "application/vnd.pecureo.bacchus+json; version=1",
          Authorization: `Bearer ${authorization_token}`
        },
        method
      };

      if (Object.keys(params).length > 0) {
        if (endpoint.indexOf("?") < 0) {
          uri += `?${queryStringify(params, { arrayFormat: "brackets" })}`;
        } else {
          uri += `&${queryStringify(params, { arrayFormat: "brackets" })}`;
        }
      }

      if (Object.keys(body).length > 0) {
        payload["body"] = JSON.stringify(body);
        payload.headers["Content-Type"] = "application/json; charset=utf-8";
      }

      const response = await fetch(uri, payload);

      return response.json();
    };
  }

  public stock_mutations$(params: object = {}) {
    return this.request("/api/stock_mutations.json", "get", params)
      .map((response: JsonApiDataDocument) =>
        response.data.map((stock_mutation_data: StockMutationData) =>
          new StockMutation(this, stock_mutation_data)
        )
      );
  }

  public unit_categories$(params: object = {}) {
    return this.request("/api/unit_categories.json", "get", params)
      .map((response: JsonApiDataDocument) =>
        response.data.map((unit_category_data: UnitCategoryData) =>
          new UnitCategory(this, unit_category_data)
        ));
  }

  public unit_classes$(params: object = {}) {
    return this.request("/api/unit_classes.json", "get", params)
      .map((response: JsonApiDataDocument) =>
        response.data.map((unit_class_data: UnitClassData) =>
          new UnitClass(this, unit_class_data)
        ));
  }

  public unit_types$(params: object = {}) {
    return this.request("/api/unit_types.json", "get", params)
      .map((response: JsonApiDataDocument) =>
        response.data.map((unit_type_data: UnitTypeData) =>
          new UnitType(this, unit_type_data)
        ));
  }

  public units$(params: object = {}) {
    return this.request("/api/units.json", "get", params)
      .map((response: JsonApiDataDocument) =>
        response.data.map((unit_data: UnitData) => {
          return new Unit(this, unit_data, response.included);
        })
      );
  }
}
