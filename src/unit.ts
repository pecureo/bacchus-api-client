import { ReplaySubject } from "rxjs/ReplaySubject";

import { BacchusApi } from "./bacchus_api";
import {
  isDataResponse, JsonApiDataDocument, JsonApiDocument, JsonApiResourceObject
} from "./jsonapi";
import { StockMutation, StockMutationData } from "./stock_mutation";

export class Unit {
  public available_at: Date;
  public buy: number;
  public expires_at: Date;
  public id: number;
  public metadata: object;
  public minimum: number;
  public multiple_of: number;
  public name: {
    plural: string;
    singular: string;
  };
  public sell: number;
  public tax: number;

  private _stock_mutations$: ReplaySubject<StockMutation[]> = new ReplaySubject(1);
  private _stock_mutations_loaded_once: boolean = false;

  constructor(
    private _api: BacchusApi,
    private _data: UnitData,
    related_data: JsonApiResourceObject[] = []
  ) {
    this.populateFromUnitData();

    // If data was side-loaded when the Unit was retrieved, push it into the
    // `stock_mutation$` immediately
    if (related_data.length > 0) {
      const stock_mutation_ids = (
        // Although in general the data in a relationship does not have to be
        // an array, in this case it does so it can be safely cast
        this._data.relationships["stock-mutations"].data as JsonApiResourceObject[]
      ).map((related_stock_mutation) => related_stock_mutation.id);

      // The related data may contain data not relevant to the current
      // instance, so some filtering is necessary
      this._stock_mutations$.next(related_data.filter((stock_mutation_data) =>
        stock_mutation_data.type === "stock-mutations" &&
        stock_mutation_ids.indexOf(stock_mutation_data.id) >= 0
      ).map((stock_mutation_data: StockMutationData) =>
        new StockMutation(this._api, stock_mutation_data)
      ));

      this._stock_mutations_loaded_once = true;
    }
  }

  get stock_mutations$() {
    if (!this._stock_mutations_loaded_once) {
      // In case the StockMutations haven't at least been loaded once (for
      // instance if they haven't been side-loaded retrieve them on the first
      // subscription)
      this.refreshStockMutations();
      this._stock_mutations_loaded_once = true;
    }

    return this._stock_mutations$;
  }

  public refreshStockMutations() {
    this._api.request(
      this._data.relationships["stock-mutations"].links.self as string
    ).map((response: JsonApiDataDocument) => response.data.map(
      (stock_mutation_data: StockMutationData) =>
        new StockMutation(this._api, stock_mutation_data)
    )).subscribe(this._stock_mutations$.next.bind(this._stock_mutations$));
  }

  public save() {
    const unit = {
      available_at: this.available_at.toISOString(),
      buy: this.buy,
      expires_at: this.expires_at ? this.expires_at.toISOString() : null,
      metadata: this.metadata,
      minimum: this.minimum,
      multiple_of: this.multiple_of,
      sell: this.sell,
      tax: this.tax
    };

    return this._api.request(
      this._data.links.self as string, "put", {}, { unit }
    ).map((response: JsonApiDocument) => {
      if (isDataResponse(response) && isUnitData(response.data)) {
        this._data = response.data;
        this.populateFromUnitData();
        return this;
      } else {
        this._api.error$.next(new Error("Did not receive appropriate data in response"));
      }
    });
  }

  private populateFromUnitData() {
    this.available_at = new Date(this._data.attributes["available-at"]);
    this.buy = parseFloat(this._data.attributes.buy);

    if (this._data.attributes["expires-at"]) {
      this.expires_at =  new Date(this._data.attributes["expires-at"]);
    }

    this.id = parseInt(this._data.id, 10);
    this.metadata = this._data.attributes.metadata;
    this.minimum = this._data.attributes.minimum;
    this.multiple_of =  this._data.attributes["multiple-of"];
    this.name =  {
      plural: this._data.attributes["plural-name"],
      singular: this._data.attributes.name
    };
    this.sell = parseFloat(this._data.attributes.sell);
    this.tax = parseFloat(this._data.attributes.tax);
  }
}

export interface UnitData extends JsonApiResourceObject {
  attributes: {
    "available-at": string;
    buy: string;
    "expires-at": string;
    metadata: object;
    minimum: number;
    "multiple-of": number;
    name: string;
    "plural-name": string;
    sell: string;
    tax: string;
  };
}

export function isUnitData(
  data: JsonApiResourceObject
): data is UnitData {
  return data && data.attributes && data.attributes.buy !== undefined;
}
