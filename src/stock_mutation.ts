import { BacchusApi } from "./bacchus_api";
import {
  isDataResponse, JsonApiDocument, JsonApiResourceObject
} from "./jsonapi";

export class StockMutation {
  public action: string;
  public created_at: Date;
  public created_by: string;
  public details: StockMutationDetailData[];
  public id: number;
  public metadata: object;
  public payment_status: string;

  constructor(private _api: BacchusApi, private _data: StockMutationData) {
    this.populateFromStockMutationData();
  }

  public save() {
    const stock_mutation = {
      action: this.action,
      created_by: this.created_by,
      metadata: this.metadata,
      payment_status: this.payment_status
    };

    return this._api.request(
      this._data.links.self as string, "put", {}, { stock_mutation }
    ).map((response: JsonApiDocument) => {
      if (isDataResponse(response) && isStockMutationData(response.data)) {
        this._data = response.data;
        this.populateFromStockMutationData();
        return this;
      } else {
        this._api.error$.next(new Error("Did not receive appropriate data in response"));
      }
    });
  }

  private populateFromStockMutationData() {
    this.action = this._data.attributes["action"];
    this.created_at = new Date(this._data.attributes["created-at"]);
    this.created_by = this._data.attributes["created-by"];
    this.details = this._data.attributes["details"].map((detail) => {
      detail.unit_id = detail["unit-id"];
      delete detail["unit-id"];
      return detail;
    });
    this.id = parseInt(this._data.id, 10);
    this.metadata = this._data.attributes["metadata"];
    this.payment_status = this._data.attributes["payment-status"];
  }
}

export interface StockMutationData extends JsonApiResourceObject {
  attributes: {
    action: string;
    "created-at": string;
    "created-by": string;
    details: StockMutationDetailData[];
    metadata: object;
    "payment-status": string;
  };
}

export function isStockMutationData(
  data: JsonApiResourceObject
): data is StockMutationData {
  return data && data.attributes && data.attributes.action !== undefined;
}

export interface StockMutationDetailData {
  amount: number;
  unit_id?: number;
  "unit-id"?: number;
}
