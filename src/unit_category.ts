import { Observable } from "rxjs/Observable";

import { BacchusApi } from "./bacchus_api";
import {
  JsonApiDataDocument, JsonApiLinksObject, JsonApiResourceObject
} from "./jsonapi";
import { Unit, UnitData } from "./unit";

export class UnitCategory {
  public id: number;
  public name: string;
  public position: number;

  constructor(private _api: BacchusApi, private _data: UnitCategoryData) {
    this.id = parseInt(this._data.id, 10);
    this.name = this._data.attributes.name;
    this.position = this._data.attributes.position;
  }

  public units$(params: object = {}): Observable<Unit[]> {
    return this._api.request(this._data.relationships.units.links.related as string, "get", params)
      .map((response: JsonApiDataDocument) =>
        response.data.map((unit_data: UnitData) => new Unit(this._api, unit_data))
      );
  }
}

export interface UnitCategoryData extends JsonApiResourceObject {
  attributes: {
    name: string;
    position: number;
  };

  relationships: {
    units: {
      data: UnitData[]
      links: JsonApiLinksObject;
    }
  };
}
