import { Observable } from "rxjs/Observable";

import { BacchusApi } from "./bacchus_api";
import {
  JsonApiDataDocument, JsonApiLinksObject, JsonApiResourceObject
} from "./jsonapi";
import { Unit, UnitData } from "./unit";

export class UnitType {
  public active: boolean;
  public id: number;
  public name: string;
  public quantity: string;
  public size: number;

  constructor(private _api: BacchusApi, private _data: UnitTypeData) {
    this.active = this._data.attributes.active;
    this.id = parseInt(this._data.id, 10);
    this.name = this._data.attributes.name;
    this.quantity = this._data.attributes.quantity;
    this.size = parseInt(this._data.attributes.size, 10);
  }

  public units$(params: object = {}): Observable<Unit[]> {
    return this._api.request(this._data.relationships.units.links.related as string, "get", params)
      .map((response: JsonApiDataDocument) =>
        response.data.map((unit_data: UnitData) => new Unit(this._api, unit_data))
      );
  }
}

export interface UnitTypeData extends JsonApiResourceObject {
  attributes: {
    active: boolean;
    name: string;
    quantity: string;
    size: string;
  };

  relationships: {
    units: {
      data: UnitData[]
      links: JsonApiLinksObject;
    }
  };
}
